// Christopher Brown

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "TagController.generated.h"



/**
 * 
 */
UCLASS()
class GAM312FINALPROJECT_API ATagController : public AAIController
{
	GENERATED_BODY()

public:
	void BeginPlay() override;
	
public:
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

private:
	UPROPERTY()
		TArray<AActor*> WayPoints;
	
	UFUNCTION()
		ATargetPoint* GetRandomWaypoint();

	UFUNCTION()
		void GoToRandomWaypoint();
	
private:
	FTimerHandle TimerHandle;

	
};
