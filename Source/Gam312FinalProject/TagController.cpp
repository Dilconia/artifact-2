// Christopher Brown


#include "TagController.h"
#include "AIController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

void ATagController::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), WayPoints);

	GoToRandomWaypoint();
}

ATargetPoint* ATagController::GetRandomWaypoint()
{
	auto index = FMath::RandRange(0, WayPoints.Num() - 1);
	return Cast<ATargetPoint>(WayPoints[index]);
}

void ATagController::GoToRandomWaypoint()
{
	MoveToActor(GetRandomWaypoint());
}

void ATagController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	GetWorldTimerManager().SetTimer(TimerHandle, this, &ATagController::GoToRandomWaypoint, 1.0f, false);
}